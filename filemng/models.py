# -*- coding: utf-8 -*-
#from django.db import models
# Creation date: 10.01.12
from bson.objectid import ObjectId
from django.db.utils import DatabaseError

import gridfs
import pymongo
from pymongo.errors import AutoReconnect, ConnectionFailure

def _connect_mng_grid():
    try:
        connection= pymongo.connection.Connection(host='localhost', port=27017)
    except (AutoReconnect,ConnectionFailure), err:
        raise DatabaseError(err)
    gr= gridfs.GridFS(pymongo.database.Database(connection,'filearc'))
    return gr


def save_mng_file(disk_name,filehash):
    gr= _connect_mng_grid()
    f=open(disk_name,'rb')  # f.seek(0, 0) - ??
    s_id=gr.put(f.read(),_id=filehash,filename=filehash,contentType="application/octet-stream")
    f.close()
    return s_id

def remove_mng_file(filehash):
    gr= _connect_mng_grid()
    return gr.delete(filehash)

def read_mng_file(filehash):
    """
    !!! This is not method for product deployment!
    !!! USE NGINX+GridFS direct connect
    https://github.com/mdirolf/nginx-gridfs
    """
    gr= _connect_mng_grid()
    ff=gr.get(filehash)
    return ff.read()

##db.addUser('filerc','go54Restrez')
