from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template
from django.contrib.auth.views import login, logout
from users.views import *
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
#    url(r'^$', direct_to_template,{'template': 'index.html','mimetype':'text/html'}),
    url(r'^profile/$', profile, name='profile'),
    url(r'^accounts/login/$',login),
    url(r'^upload/$', upload_file),
    url(r'^rem/(?P<filehash>[-\da-f]{30,})/(?P<filename>.+)$',remove_file),
    url(r'^get/(?P<filehash>[-\da-f]{30,})/.+',get_file),
    url(r'^accounts/new/$', create_user),
    url(r'^accounts/logout/$',logout),
    url(r'^accounts/profile/$', profile, name='profile'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^robots\.txt$', direct_to_template,{'template': 'robots.txt','mimetype':'text/plain'}),
    url(r'^time/$', xhr_test ),
    # Apache intercepts this call! Development-serve serves
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    #url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}), # why its not working?
    url(r'^about/.*$',say404_nolog),
    url(r'^.+$', say404),
)

