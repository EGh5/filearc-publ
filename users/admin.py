#__author__ = 'Eugene'
from users.models import StoredFile,FileItem,UserBasket
from django.contrib import admin


class StoredFileAdmin(admin.ModelAdmin):
    list_display = ('filehash','ucounter','filesize','date_created')

admin.site.register(StoredFile,StoredFileAdmin)

class FileStoredAdmin(admin.ModelAdmin):
    list_display=('file_stored','name','user','date_created')

admin.site.register(FileItem,FileStoredAdmin)

class UserBasketAdmin(admin.ModelAdmin):
    list_display=('username','date_created','file_counter')

admin.site.register(UserBasket,UserBasketAdmin)

