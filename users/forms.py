# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext as _

class UploadFileForm(forms.Form):
    #title = forms.CharField(max_length=50)
    file  = forms.FileField()

class NewUserForm(forms.Form):
    username = forms.CharField(label=_(u'Username'),min_length=1, max_length=30)
    password = forms.CharField(label=_(u'Password'), widget=forms.PasswordInput, min_length=3)

