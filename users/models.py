# -*- coding: utf8 -*-
from django.db import models
"""
Модель с реализацией many-to-many по мотивам
https://docs.djangoproject.com/en/1.3/topics/db/models/#extra-fields-on-many-to-many-relationships
Запросы к FileItem делаем так FileItem.objects.filter(user__username=request.user.username,
file_stored__filehash=f.filehash) двойное подчеркивание (неиспользовать в полях!) означает -
поле в FileItem--->поле в соответствующем классе куда показывает ForeignKey
"""
class StoredFile(models.Model):                                             # модель - целевой класс
    filehash = models.CharField("хэш-имя файла", max_length=50)
    ucounter =models.IntegerField("счетчик использований",default=0)                    #TODO подумать .count()?
    filesize = models.IntegerField("размер файла")
    date_created=models.DateTimeField("дата создания",null=True,auto_now=True)
    def __unicode__(self):
        return "%s" % self.filehash

class UserBasket(models.Model):                                             # модель класс-источник
    username = models.CharField("логин",max_length=30)
    files = models.ManyToManyField(StoredFile, through='FileItem', null=True, blank=True)
    date_created=models.DateTimeField("дата создания",null=True,auto_now=True)

    def _get_item_counter(self):
        return self.files.count()
    file_counter=property(_get_item_counter,None,None,'счетчик файлов')

    def __unicode__(self):
        return "%s : %i" % (self.username,self.file_counter)


class FileItem(models.Model):                                                # модель-посредник
    file_stored = models.ForeignKey(StoredFile,verbose_name="ссылка на файл")# один и только один ключ на источник-модель
    user = models.ForeignKey(UserBasket, verbose_name="ссылка на корзину")   # один и только один ключ на цель-модель
    name = models.CharField("имя файла юзера",max_length=200)
    date_created=models.DateTimeField("дата создания",null=True,auto_now=True)

    def __unicode__(self):
        return "%s : %s" % (self.file_stored,self.name)





