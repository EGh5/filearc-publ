# -*- coding: utf-8 -*-
import re
from django.contrib.auth.decorators import login_required
from django.core.exceptions import MultipleObjectsReturned
from django.db.utils import IntegrityError
from django.http import HttpResponse, HttpResponseNotFound, Http404
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic.simple import direct_to_template, redirect_to
from django.contrib.auth.models import User
from django.contrib import auth
from forms import *
from django.conf import settings
from models import *
import hashlib,zlib,os
from filemng.models import *
import logging
log = logging.getLogger(__name__)

#store_root  = settings.STORE_PATH
max_file_num= settings.MAX_FILE_NUM
media_root  = settings.MEDIA_ROOT

def xhr_test(request):  # this view should be moved in another application!
        import time
        from subprocess import Popen, PIPE
        if request.is_ajax():
            message= "Время сервера:  "+time.strftime("%d.%m.%Y  %H:%M:%S")
            try:
                x=open("/var/lib/boinc-client/client_state.xml",'r')
            except (OSError,IOError), err:
                return HttpResponse("Can't find client_state.xml! %s" % err)
            ss=x.readlines(); x.close()
            ss="".join(ss)
            r1=re.compile(r'<user_total_credit>(.+)</user_total_credit>')
            r2=re.compile(r'<last_update>(.+)</last_update>')
            r3=re.compile(r'<checkpoint_fraction_done>(.+)</checkpoint_fraction_done>')
            r4=re.compile(r'<user_friendly_name>(.+)</user_friendly_name>')

            s=r1.findall(ss)
            s2=r2.findall(ss)
            s3=r3.findall(ss); s3=" | ".join(s3)
            s4=r4.findall(ss);s4=", ".join(s4)
            s=("".join(s))
            s=float(s)
            #r1.finditer
            message+=".  Заработанные очки по проекту: <b>%.1f </b>"%float(s)
            message+="<br />Последнее обновление: "+ time.strftime("%d.%m.%Y  %H:%M:%S",time.localtime(float("".join(s2))))
            message+=".  Активные задания выполнены на <b> "+s3+"</b> процентов.<br />Загруженные приложения: <br />"+ s4+"<br />\n"

        else:
            message = "Hello"
            pass
        return HttpResponse(message)


def set_record():
    class record:
        def __init__(self):
            self.name=""
            self.filesize=""
            self.filehash=""
    x=record()
    return x

def say404(request):
    sreferer=request.META.get('HTTP_REFERER',"n/a")
    sagent  =request.META.get('HTTP_USER_AGENT',"n/a")
    saddr=request.META.get('REMOTE_ADDR',"n/a")
    log.error("Error 404 %s, %s, %s, %s" % (request.path,sreferer,sagent,saddr))
    raise Http404()

def say404_nolog(request):
    raise Http404()

def get_file(request,filehash):
    """
    Getting file by http-request - filehash+name (name is not used while)
    !!! This is not method for product deployment!
     !!! USE NGINX+GridFS direct connect
    """
    if not StoredFile.objects.get(filehash=filehash):
        say404(request)
    try:
        log.info(u"Показываем файл " + filehash)
        return HttpResponse(read_mng_file(filehash),mimetype="application/octet-stream")
    except DatabaseError:
        raise Http404()

@login_required(login_url='/accounts/login/')
def profile(request):
    """
        показываем личную страницу
    """
    mfn=max_file_num  # for Context
    ub=UserBasket.objects.get(username=request.user.username)
    try:
        files_list=ub.fileitem_set.all()# получили список объектов типа FileItem в объекте UserBasket юзера
    except DoesNotExist:
        pass
    if len(files_list)>0:
        filelist=[]
        for f in files_list:
            x1=set_record()
            x1.name=f.name
            x1.filesize="%6.1f Kb"% (StoredFile.objects.get(filehash=f.file_stored).filesize / 1000.)
            x1.filehash=f.file_stored
            filelist.append(x1)
    file_counter=ub.file_counter  # переменная для контекста
    if file_counter< max_file_num:
        fupl_html=UploadFileForm()
    return direct_to_template(request, 'users/profile.html', locals())

@login_required(login_url='/accounts/login/')
def remove_file(request,filehash,filename):
    """
    Удаляем файл по хэшу и имени. Если файл используется другим пользователем
    только убавляем ему счетчик
    """
    ub=UserBasket.objects.get(username=request.user.username)
    # files_list=ub.fileitem_set.all() # получили список объектов типа FileItem в экз. UserBasket
    try:
        fi=FileItem.objects.get(user__username=request.user.username,
                            file_stored__filehash=filehash,name=filename).delete
    except FileItem.DoesNotExist:
        success_msg=u"Повторный запрос? Вы ранее уже удалили файл %s" % filename
        return direct_to_template(request, 'users/success_file_delete.html', locals())
    sf=StoredFile.objects.get(filehash=filehash)
    sf.ucounter-=1; sf.save()
    if sf.ucounter == 0:
        try:
            remove_mng_file(filehash)
            sf.delete()
        except DatabaseError:
            success_msg=u"Ошибка удаления файла %s из хранилища" % filename
            return direct_to_template(request, 'users/success_file_delete.html', locals())
    success_msg=u"Вы удалили файл %s" % filename
    return direct_to_template(request, 'users/success_file_delete.html', locals())

def create_user(request):
    fnew=NewUserForm(request.POST or None)
    if request.method != 'POST':
        return direct_to_template(request, 'users/user_new.html', locals())
    if not fnew.is_valid():
        error_msg=u"Проверьте введенные данные!"
        return direct_to_template(request, 'users/error_new.html', locals())
    usr = fnew.cleaned_data.get('username', None)
    psw = fnew.cleaned_data.get('password', None)
    try: # existing users?
        User.objects.get(username=usr)
        error_msg="User exists!"
        return direct_to_template(request, 'users/error_new.html', locals())
    except User.DoesNotExist:
        try: # create new user record
            user=User.objects.create_user(username=usr, password=psw, email='')
            user.save()
        except IntegrityError:
            return direct_to_template(request, 'users/error_new.html', locals())
        # create basket and login
        ub=UserBasket(username=usr)
        ub.save()
        user=auth.authenticate(username=usr, password=psw)
        auth.login(request,user)
    return direct_to_template(request, 'users/success_create.html', locals())

def handle_uploaded_file(f,name):
    """
    Вспомогательная процедура приема файла из сети и сохранения на диске
    в директории media.
    Были проблемы с русской кодировкой принимаемых файлов - пришлось сделать перекодирование
    в koi8-r
    Возвращает новое название=хэш, размер файла в байтах и полный путь до сохраненного файла на диске
    """
    name=media_root+name.encode("koi8-r",'replace')
    destination = open(name, 'wb+')
    m = hashlib.md5()
    crc_32=0
    for chunk in f.chunks():
        destination.write(chunk)
        m.update(chunk)
        crc_32=zlib.crc32(chunk, crc_32)
    destination.close()
    filesize=os.path.getsize(name)
    crc_32 = u"%s" % (crc_32 & 0xffffffff)  # positive value is simpler
    mmm=m.hexdigest()                       # hexadecimal in obvious style
    filehash=mmm+'-'+crc_32
    return (filehash, filesize, name)

@login_required(login_url='/accounts/login/')
def upload_file(request):
    if request.method == 'POST':
        fupl_html = UploadFileForm(request.POST, request.FILES)
        try:
            user_file_name=request.FILES['file'].name
        except MultiValueDictKeyError:
            error_msg=u"Похоже, Вы забыли указать файл для загрузки"
            return direct_to_template(request, 'users/error_file_new.html', locals())
        if fupl_html.is_valid():
            (filehash2, filesize2, name_downloaded )=handle_uploaded_file(request.FILES['file'],user_file_name)
            ub=UserBasket.objects.get(username=request.user.username)
            try: # existing file?
                f=StoredFile.objects.get(filehash=filehash2)
                if f.filesize != filesize2:
                    error_msg=u"Одинаковый хэш, разные размеры!"                # TODO подумать - такое может быть?
                    os.remove(name_downloaded)
                    return direct_to_template(request, 'users/error_file_new.html', locals())
                fi_previous_user=FileItem.objects.filter(file_stored__filehash=filehash2)
                previous_file_list=[]
                for fi_prev in fi_previous_user:
                    x1=set_record()
                    x1.name=fi_prev.name
                    x1.username=fi_prev.user.username
                    previous_file_list.append(x1)
                try:
                    fi=FileItem.objects.get(user__username=request.user.username,
                            file_stored__filehash=filehash2,name=user_file_name)
                    os.remove(name_downloaded)
                    error_msg=u"У вас такой файл и с таким названием уже есть на нашем сайте!"
                    return direct_to_template(request, 'users/error_file_new.html', locals())
                except MultipleObjectsReturned:
                    os.remove(name_downloaded)
                    error_msg=u"У вас таких файлов уже несколько!! (???)"
                    return direct_to_template(request, 'users/error_file_new.html', locals())
                except FileItem.DoesNotExist:
                        pass
            except StoredFile.DoesNotExist:
                f=StoredFile.objects.create(filehash=filehash2,filesize=filesize2)
                try:
                    save_mng_file(name_downloaded,filehash2)
                    os.remove(name_downloaded)
                except DatabaseError:                  # TODO file rename error handling ?
                    error_msg=u"Приносим извинения, ошибка сохранения файла!"
                    return direct_to_template(request, 'users/error_file_new.html', locals())
                    pass                                                     # нужно понаблюдать
            f.ucounter+=1; f.save()
            mm=FileItem(file_stored=f,user=ub,name=request.FILES['file'].name); mm.save()
            ### vars for Context ###
            mfn=max_file_num
            file_counter=ub.file_counter
            return direct_to_template(request, 'users/file_upload.html', locals())
    else:
        fupl_html = UploadFileForm(request)
    return profile(request)


